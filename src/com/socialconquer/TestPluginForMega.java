package com.socialconquer;

import com.socialconquer.profilecontroller.bukkit.ProfilePlugin;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.ConcurrentHashMap;


public class TestPluginForMega extends JavaPlugin implements Listener
{
    public void onEnable()
    {
        System.out.println("Test Plugin for Megamichiel");
        Bukkit.getPluginManager().registerEvents(this, this);
        System.out.println("Done loading!");
    }
    private ConcurrentHashMap<String, Integer> data = new ConcurrentHashMap<String, Integer>();
    public void onDisable()
    {
        Bukkit.getScheduler().cancelTasks(this);
    }
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent e)
    {
        for(OfflinePlayer p : Bukkit.getOfflinePlayers()) // Gets every player that has ever played the server
        {
            data.put(p.getName(), ProfilePlugin.getInstance().getProfile(p.getPlayer()).getInt("column")); // Puts the score of every player in a hashmap
        }
        // Grab the hashmap data and do the leaderboard stuff here?
    }



}
